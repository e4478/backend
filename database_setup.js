db = new Mongo().getDB("e-voting");

db.createCollection("roles")
db.createCollection("users")
db.createCollection("elections")

var admin_role = ObjectId()
var user_role = ObjectId()

db.roles.insert({
"name": "ROLE_ADMIN",
"_id": admin_role
})
db.roles.insert({
"name": "ROLE_USER",
"_id": user_role
})

db.users.insert({
"firstName": "admin",
"lastName": "admin",
"email": "admin",
"password": "$2b$12$7MfdyNJ3wQS9DZCpffNtb.UrdCnTNwUNi5BoFtRnew6D6rLgtYXO.",
"roles": [ {"$ref": "roles", "$id": admin_role}, {"$ref": "roles", "$id": user_role} ],
"_class" : "com.example.evoting.models.User"
})
