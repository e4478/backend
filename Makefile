all: remove_containers compile start

remove_containers:
	docker compose rm -f backend
	docker compose rm -f database

compile: target/e-voting-0.0.1-SNAPSHOT.jar
	mvn clean install -DskipTests

start:
	docker compose build
	docker compose up
