package com.example.evoting.repositories;

import com.example.evoting.models.Election;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.*;

public interface ElectionRepository extends MongoRepository<Election, String> {

    List<Election> findAll();

    Optional<Election> findById(String id);

}
