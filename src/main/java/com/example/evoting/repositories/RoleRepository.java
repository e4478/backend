package com.example.evoting.repositories;

import com.example.evoting.models.ERole;
import com.example.evoting.models.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RoleRepository extends MongoRepository<Role, String> {

    Role findByName(ERole name);

}
