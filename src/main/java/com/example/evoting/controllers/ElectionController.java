package com.example.evoting.controllers;

import com.example.evoting.models.*;

import com.example.evoting.models.key_service_responses.KeyResponse;
import com.example.evoting.models.key_service_responses.ResultResponse;
import com.example.evoting.models.requests.AddElectionRequest;
import com.example.evoting.models.requests.VoteRequest;
import com.example.evoting.models.responses.ElectionAddResponse;
import com.example.evoting.models.responses.UserGetElectionsResponse;
import com.example.evoting.repositories.*;
import com.example.evoting.security.JwtUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/election")
public class ElectionController {
    private final String INVALID_TOKEN_MESSAGE = "Invalid token";
    private final String ELECTION_ADDED_MESSAGE = "Election added";
    private final String USER_NOT_AUTHORIZED_MESSAGE = "User not authorized";
    private final String INVALID_ELECTION_MESSAGE = "Invalid election";
    private final String VOTE_CASTED_SUCCESSFULLY_MESSAGE = "Vote casted successfully";
    private final String INVALID_VOTE_MESSAGE = "Invalid vote";
    private final String NOT_PERMITTED_MESSAGE = "You are not permitted to do this";
    private final String INVALID_DATES_MESSAGE = "End date must be after start date and election must end in the future";
    private final String VOTE_NOT_IN_TIME_FRAME = "You cannot vote now in this election";
    private final String VOTE_ALREADY_CASTED_MESSAGE = "You already casted a vote in this election";
    private final String CANNOT_GET_RESULTS_BEFORE_END_MESSAGE = "You cannot get results while election is in progress";

    @Value("${keyServiceHostName}")
    private String KEY_SERVICE_HOST_NAME;

    @Autowired
    ElectionRepository electionRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    JwtUtils jwtUtils;

    private int nextPrime(int input){
        int counter;
        input++;
        while(true){
            int l = (int) Math.sqrt(input);
            counter = 0;
            for(int i = 2; i <= l; i ++){
                if(input % i == 0)  counter++;
            }
            if(counter == 0)
                return input;
            else{
                input++;
            }
        }
    }

    @RequestMapping(value = "elections", method = RequestMethod.GET)
    public ResponseEntity<?> getElections(@RequestHeader(name = "Token") String token) {
        if (!jwtUtils.validateJwtToken(token)) {
            return ResponseEntity.badRequest().body(INVALID_TOKEN_MESSAGE);
        }
        Optional<User> userOptional = userRepository.findByEmail(jwtUtils.getUserNameFromJwtToken(token));
        if (userOptional.isEmpty()) {
            return ResponseEntity.badRequest().body(USER_NOT_AUTHORIZED_MESSAGE);
        }

        User user = userOptional.get();
        if (user.getRoles().stream().anyMatch(r -> r.getName().equals(ERole.ROLE_ADMIN))) {
            return ResponseEntity.ok(electionRepository.findAll());
        }

        return ResponseEntity.ok(electionRepository.findAll().stream().map(UserGetElectionsResponse::new).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<?> addElection(@RequestBody AddElectionRequest body, @RequestHeader(name = "Token") String token) {
        if (!jwtUtils.validateJwtToken(token)) {
            return ResponseEntity.badRequest().body(INVALID_TOKEN_MESSAGE);
        }
        Optional<User> userOptional = userRepository.findByEmail(jwtUtils.getUserNameFromJwtToken(token));
        if (userOptional.isEmpty()) {
            return ResponseEntity.badRequest().body(USER_NOT_AUTHORIZED_MESSAGE);
        }
        User user = userOptional.get();
        if (user.getRoles().stream().noneMatch(r -> r.getName().equals(ERole.ROLE_ADMIN))) {
            return ResponseEntity.badRequest().body(NOT_PERMITTED_MESSAGE);
        }

        //date validation
        if (body.getStartDate().after(body.getEndDate()) || body.getEndDate().before(new Date())) {
            return ResponseEntity.badRequest().body(INVALID_DATES_MESSAGE);
        }
        //generating prime numbers for candidates
        Map<Integer, String> choices = new HashMap<>();
        int primeNumber = 1;
        for (String candidate : body.getCandidates()) {
            primeNumber = nextPrime(primeNumber);
            choices.put(primeNumber, candidate);
        }

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        ObjectId id = new ObjectId();

        String requestBody = String.format("{\"key_id\": \"%s\"}", id);
        System.out.println(requestBody);

        HttpEntity<String> httpEntity = new HttpEntity<>(requestBody, headers);
        KeyResponse response = restTemplate.postForObject(String.format("http://%s:5000/key_gen", KEY_SERVICE_HOST_NAME), httpEntity, KeyResponse.class);

        if (response != null) {
            Election election = new Election(id.toString(), body.getStartDate(), body.getEndDate(), choices, response.getP(), response.getG(), response.getY());
            electionRepository.save(election);
            return ResponseEntity.ok(new ElectionAddResponse(ELECTION_ADDED_MESSAGE, election));
        }

        return ResponseEntity.internalServerError().body("Error");
    }

    @RequestMapping(value = "/vote", method = RequestMethod.POST)
    public ResponseEntity<?> vote(@RequestBody VoteRequest body, @RequestHeader(name = "Token") String token) throws NoSuchAlgorithmException {
        if (!jwtUtils.validateJwtToken(token)) {
            return ResponseEntity.badRequest().body(INVALID_TOKEN_MESSAGE);
        }

        Optional<User> userOptional = userRepository.findByEmail(jwtUtils.getUserNameFromJwtToken(token));
        if (userOptional.isEmpty()) {
            return ResponseEntity.badRequest().body(USER_NOT_AUTHORIZED_MESSAGE);
        }

        User user = userOptional.get();
        if (user.getRoles().stream().noneMatch(r -> r.getName().equals(ERole.ROLE_USER))) {
            return ResponseEntity.badRequest().body(NOT_PERMITTED_MESSAGE);
        }

        Optional<Election> electionOptional = electionRepository.findById(body.getElectionId());
        if (electionOptional.isEmpty()) {
            return ResponseEntity.badRequest().body(INVALID_ELECTION_MESSAGE);
        }

        Election election = electionOptional.get();

        Date now = new Date();
        if (now.before(election.getStartDate()) || now.after(election.getEndDate())) {
            System.out.println(election.getStartDate());
            System.out.println(now);
            System.out.println(election.getEndDate());
            return ResponseEntity.badRequest().body(VOTE_NOT_IN_TIME_FRAME);
        }

        if (election.getVotes().containsKey(user.getEmail())) {
            return ResponseEntity.badRequest().body(VOTE_ALREADY_CASTED_MESSAGE);
        }

        ArrayList<BigInteger> uBigInteger = body.getU().stream().map(BigInteger::new).collect(Collectors.toCollection(ArrayList::new));
        ArrayList<BigInteger> wBigInteger = body.getW().stream().map(BigInteger::new).collect(Collectors.toCollection(ArrayList::new));
        ArrayList<BigInteger> ApBigInteger = body.getAp().stream().map(BigInteger::new).collect(Collectors.toCollection(ArrayList::new));
        ArrayList<BigInteger> BpBigInteger = body.getBp().stream().map(BigInteger::new).collect(Collectors.toCollection(ArrayList::new));

        if (!verify(new BigInteger(election.getP()), new BigInteger(election.getG()), new BigInteger(election.getY()), uBigInteger, wBigInteger, ApBigInteger, BpBigInteger, election.getChoices().keySet(), body.getEncryptedVote())) {
            return ResponseEntity.badRequest().body(INVALID_VOTE_MESSAGE);
        }

        election.getVotes().put(user.getEmail(), body.getEncryptedVote());
        electionRepository.save(election);

        return ResponseEntity.ok(VOTE_CASTED_SUCCESSFULLY_MESSAGE);
    }

    @RequestMapping(value = "/election", method = RequestMethod.GET)
    public ResponseEntity<?> results(@RequestParam String electionId, @RequestHeader(name = "Token") String token) {
        if (!jwtUtils.validateJwtToken(token)) {
            return ResponseEntity.badRequest().body(INVALID_TOKEN_MESSAGE);
        }

        Optional<User> userOptional = userRepository.findByEmail(jwtUtils.getUserNameFromJwtToken(token));
        if (userOptional.isEmpty()) {
            return ResponseEntity.badRequest().body(USER_NOT_AUTHORIZED_MESSAGE);
        }

        User user = userOptional.get();
        Optional<Election> electionOptional = electionRepository.findById(electionId);
        if (electionOptional.isEmpty()) {
            return ResponseEntity.badRequest().body(INVALID_ELECTION_MESSAGE);
        }

        Election election = electionOptional.get();
        if (new Date().before(election.getEndDate())) {
            return ResponseEntity.badRequest().body(CANNOT_GET_RESULTS_BEFORE_END_MESSAGE);
        }

        EncryptedVote[] votes = election.getVotes().values().toArray(EncryptedVote[]::new);
        EncryptedVote result = votes[0];
        for (int i = 1; i < votes.length; i++) {
            result.multiply(votes[i]);
        }

        RestTemplate restTemplate = new RestTemplate();

        StringBuilder parameters = new StringBuilder();
        parameters.append("?key_id=");
        parameters.append(electionId);
        parameters.append("&a=");
        parameters.append(result.getA());
        parameters.append("&b=");
        parameters.append(result.getB());

        ResultResponse aggregatedResult = restTemplate.getForObject(String.format("http://%s:5000/decrypt", KEY_SERVICE_HOST_NAME) + parameters, ResultResponse.class);

        BigInteger r = new BigInteger(aggregatedResult.getAggregatedResult());

        Map<String, Integer> results = new HashMap<>();
        for (Map.Entry<Integer, String> choice : election.getChoices().entrySet()) {
            int counter = 0;
            BigInteger div = new BigInteger(choice.getKey().toString());
            while (r.mod(div).toString().equals("0")) {
                r = r.divide(div);
                counter++;
            }
            results.put(choice.getValue(), counter);
        }

        return ResponseEntity.ok(results);
    }

    private boolean verify(BigInteger p, BigInteger g, BigInteger y, ArrayList<BigInteger> u, ArrayList<BigInteger> w, ArrayList<BigInteger> Ap, ArrayList<BigInteger> Bp, Set<Integer> primes, EncryptedVote encryptedVote) throws NoSuchAlgorithmException {
        BigInteger sumU = sum(u);

        ArrayList<BigInteger> values = new ArrayList<>();
        values.addAll(Ap);
        values.addAll(Bp);
        BigInteger hash = hash(values);

        if (!hash.equals(sumU)) {
            return false;
        }

        int k = primes.size();

        BigInteger a = new BigInteger(encryptedVote.getA());
        BigInteger b = new BigInteger(encryptedVote.getB());

        ArrayList<BigInteger> ApCheck = new ArrayList<>();
        ArrayList<BigInteger> BpCheck = new ArrayList<>();

        Iterator<Integer> iter = primes.iterator();

        for (int j = 0; j < k; j++) {
            BigInteger firstA = g.modPow(w.get(j), p);
            BigInteger secondA = a.modPow(u.get(j), p);

            BigInteger sj = BigInteger.valueOf(iter.next());

            BigInteger firstB = y.modPow(w.get(j), p);
            BigInteger secondB = sj.modInverse(p).multiply(b).modPow(u.get(j), p);

            ApCheck.add(firstA.multiply(secondA).mod(p));
            BpCheck.add(firstB.multiply(secondB).mod(p));
        }

        return ApCheck.equals(Ap) && BpCheck.equals(Bp);
    }

    private BigInteger hash(ArrayList<BigInteger> values) throws NoSuchAlgorithmException {
        StringBuilder originalString = new StringBuilder();
        values.forEach(v -> originalString.append(v.toString()));

        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hashBytes = digest.digest(originalString.toString().getBytes(StandardCharsets.UTF_8));

        return new BigInteger(hashBytes);
    }

    private BigInteger sum(ArrayList<BigInteger> values) {
        BigInteger result = BigInteger.ZERO;

        for (BigInteger value : values) {
            result = result.add(value);
        }

        return result;
    }

//    public static BigInteger decrypt(EncryptedVote vote, Election election) {
//        BigInteger a = new BigInteger(vote.getA()).modPow(new BigInteger(election.getX()), new BigInteger(election.getP()));
//        BigInteger b = a.modInverse(new BigInteger(election.getP()));
//        return b.multiply(new BigInteger(vote.getB())).mod(new BigInteger(election.getP()));
//    }

}
