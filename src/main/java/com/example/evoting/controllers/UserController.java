package com.example.evoting.controllers;

import com.example.evoting.models.ERole;
import com.example.evoting.models.User;
import com.example.evoting.models.requests.ChangePasswordRequest;
import com.example.evoting.models.requests.LoginRequest;
import com.example.evoting.models.requests.RegisterRequest;
import com.example.evoting.models.responses.LoginResponse;
import com.example.evoting.repositories.RoleRepository;
import com.example.evoting.repositories.UserRepository;
import com.example.evoting.security.JwtUtils;
import com.example.evoting.security.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/")
@CrossOrigin
public class UserController {

    private static final String EMAIL_PATTERN = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
    private static final String PASSWORD_PATTERN = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$";

    private static final String INVALID_EMAIL_MESSAGE = "Invalid e-mail";
    private static final String INVALID_PASSWORD_MESSAGE = "Password must contain minimum eight characters, at least one letter and one number";
    private static final String EMAIL_IN_USE_MESSAGE = "E-mail address already in use";
    private static final String ACCOUNT_CREATED_MESSAGE = "Account created";
    private static final String BAD_CREDENTIALS_MESSAGE = "Invalid e-mail or password";
    private static final String INVALID_TOKEN_MESSAGE = "Invalid token";
    private static final String USER_NOT_AUTHORIZED_MESSAGE = "User not authorized";
    private static final String INVALID_OLD_PASSWORD_MESSAGE = "Invalid old password";
    private static final String OLD_PASSWORD_EQUALS_NEW_MESSAGE = "Old and new password must be different";
    private static final String PASSWORD_CHANGED_MESSAGE = "Password changed successfully";

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtUtils jwtUtils;

    @RequestMapping(value = "register", method = RequestMethod.POST)
    public ResponseEntity<?> register(@Valid @RequestBody RegisterRequest request) {
        if (notValid(request.getEmail(), EMAIL_PATTERN)) {
            return ResponseEntity.badRequest().body(INVALID_EMAIL_MESSAGE);
        } else if (notValid(request.getPassword(), PASSWORD_PATTERN)) {
            return ResponseEntity.badRequest().body(INVALID_PASSWORD_MESSAGE);
        } else if (userRepository.existsByEmail(request.getEmail())) {
            return ResponseEntity.badRequest().body(EMAIL_IN_USE_MESSAGE);
        }

        User user = new User(request.getFirstName(), request.getLastName(), request.getEmail(), encoder.encode(request.getPassword()));
        user.setRole(Collections.singleton(roleRepository.findByName(ERole.ROLE_USER)));
        userRepository.save(user);

        return ResponseEntity.ok(ACCOUNT_CREATED_MESSAGE);
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public ResponseEntity<?> login(@Valid @RequestBody LoginRequest request) {
        Authentication authentication;

        try {
            authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
        } catch (AuthenticationException e) {
            return ResponseEntity.badRequest().body(BAD_CREDENTIALS_MESSAGE);
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        LoginResponse loginResponse = new LoginResponse(token, userDetails.getId(), userDetails.getFirstName(), userDetails.getLastName(), userDetails.getEmail(), userDetails.getAuthorities().toString());
        return ResponseEntity.ok(loginResponse);
    }

    @RequestMapping(value = "changePassword", method = RequestMethod.POST)
    public ResponseEntity<?> changePassword(@RequestHeader(name = "Token") String token, @Valid @RequestBody ChangePasswordRequest request) {
        if (!jwtUtils.validateJwtToken(token)) {
            return ResponseEntity.badRequest().body(INVALID_TOKEN_MESSAGE);
        }
        Optional<User> userOptional = userRepository.findByEmail(jwtUtils.getUserNameFromJwtToken(token));
        if (userOptional.isEmpty()) {
            return ResponseEntity.badRequest().body(USER_NOT_AUTHORIZED_MESSAGE);
        }
        User user = userOptional.get();

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(jwtUtils.getUserNameFromJwtToken(token), request.getOldPassword()));
        } catch (AuthenticationException e) {
            return ResponseEntity.badRequest().body(INVALID_OLD_PASSWORD_MESSAGE);
        }

        if (request.getOldPassword().equals(request.getNewPassword())) {
            return ResponseEntity.badRequest().body(OLD_PASSWORD_EQUALS_NEW_MESSAGE);
        }

        if (notValid(request.getNewPassword(), PASSWORD_PATTERN)) {
            return ResponseEntity.badRequest().body(INVALID_PASSWORD_MESSAGE);
        }

        user.setPassword(encoder.encode(request.getNewPassword()));
        userRepository.save(user);
        return ResponseEntity.ok(PASSWORD_CHANGED_MESSAGE);
    }

    private boolean notValid(String text, String pattern) {
        Pattern compiled = Pattern.compile(pattern);
        Matcher matcher = compiled.matcher(text);

        return !matcher.find();
    }

}
