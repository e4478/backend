package com.example.evoting.models.requests;

import com.example.evoting.models.EncryptedVote;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;

public class VoteRequest {
    @NotBlank
    private String electionId;
    @NotBlank
    private EncryptedVote encryptedVote;
    @NotBlank
    private ArrayList<String> u;
    @NotBlank
    private ArrayList<String> w;
    @NotBlank
    private ArrayList<String> ap;
    @NotBlank
    private ArrayList<String> bp;

    public String getElectionId() {
        return electionId;
    }

    public EncryptedVote getEncryptedVote() {
        return encryptedVote;
    }

    public ArrayList<String> getU() {
        return u;
    }

    public ArrayList<String> getW() {
        return w;
    }

    public ArrayList<String> getAp() {
        return ap;
    }

    public ArrayList<String> getBp() {
        return bp;
    }
}
