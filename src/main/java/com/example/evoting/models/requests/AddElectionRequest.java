package com.example.evoting.models.requests;

import javax.validation.constraints.NotBlank;
import java.util.*;

public class AddElectionRequest {
    @NotBlank
    private Date startDate;
    @NotBlank
    private Date endDate;
    private Set<String> candidates;

    public @NotBlank Date getStartDate() {
        return startDate;
    }

    public @NotBlank Date getEndDate() {
        return endDate;
    }

    public Set<String> getCandidates() {
        return candidates;
    }
}
