package com.example.evoting.models.responses;

import com.example.evoting.models.Election;

import java.time.Instant;
import java.util.Date;
import java.util.Map;

public class UserGetElectionsResponse {
    private String id;
    private Date startDate;
    private Date endDate;
    private Map<Integer, String> choices;
    private final String p;
    private final String g;
    private final String y;

    public UserGetElectionsResponse(Election election) {
        this.id = election.getId();
        this.startDate = election.getStartDate();
        this.endDate = election.getEndDate();
        this.choices = election.getChoices();
        this.p = election.getP();
        this.g = election.getG();
        this.y = election.getY();
    }

    public String getId() {
        return id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public Map<Integer, String> getChoices() {
        return choices;
    }

    public String getP() {
        return p;
    }

    public String getG() {
        return g;
    }

    public String getY() {
        return y;
    }
}
