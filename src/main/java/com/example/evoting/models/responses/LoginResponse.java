package com.example.evoting.models.responses;

import com.example.evoting.models.Role;
public class LoginResponse {

    private String token;
    private String type;
    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private String role;

    public LoginResponse(String token, String id, String firstName, String lastName, String email, String role) {
        this.token = token;
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.role = role;
        this.type = "Bearer";
    }

    public String getToken() {
        return token;
    }

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getRole() {
        return role;
    }
}
