package com.example.evoting.models.responses;

import com.example.evoting.models.Election;

public class ElectionAddResponse {
    private String message;
    private Election election;

    public ElectionAddResponse(String message, Election election) {
        this.message = message;
        this.election = election;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Election getElection() {
        return election;
    }

    public void setElection(Election election) {
        this.election = election;
    }
}
