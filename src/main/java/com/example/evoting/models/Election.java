package com.example.evoting.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Document(collection = "elections")
public class Election {
    @Id
    private String id;
    @NotBlank
    private Date startDate;
    @NotBlank
    private Date endDate;
    private Map<Integer, String> choices;
    private Map<String, EncryptedVote> votes;
    private final String p;
    private final String g;
    private final String y;

    public Election(@NotBlank String id, @NotBlank Date startDate, @NotBlank Date endDate, Map<Integer, String> choices, String p, String g, String y) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.choices = choices;
        this.votes = new HashMap<>();
        this.p = p;
        this.g = g;
        this.y = y;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public @NotBlank Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public @NotBlank Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Map<Integer, String> getChoices() {
        return choices;
    }

    public void setChoices(Map<Integer, String> choices) {
        this.choices = choices;
    }

    public Map<String, EncryptedVote> getVotes() {
        return votes;
    }

    public void setVotes(Map<String, EncryptedVote> votes) {
        this.votes = votes;
    }

    public String getP() {
        return p;
    }

    public String getG() {
        return g;
    }

    public String getY() {
        return y;
    }

}
