package com.example.evoting.models;

import java.math.BigInteger;

public class EncryptedVote {
    private String a;
    private String b;

    public EncryptedVote(String a, String b) {
        this.a = a;
        this.b = b;
    }

    public void multiply(EncryptedVote vote) {
        this.a = new BigInteger(a).multiply(new BigInteger(vote.a)).toString();
        this.b = new BigInteger(b).multiply(new BigInteger(vote.b)).toString();
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }
}
