package com.example.evoting.models.key_service_responses;

public class ResultResponse {
    private String aggregatedResult;

    public String getAggregatedResult() {
        return aggregatedResult;
    }

    public void setAggregatedResult(String aggregatedResult) {
        this.aggregatedResult = aggregatedResult;
    }
}
